let tracks = [
    {   url : './audio/Ghostrifter Official - Still Awake.mp3' , 
        cover : './cover/cover-5.jpeg', artist : 'Ghostrifter' , title : 'Still Awake'},
    {   url : './audio/Ghostrifter Official - Midnight Stroll.mp3' , 
        cover : './cover/cover-3.jpg', artist : 'Ghostrifter' , title : 'Midnight Stroll'},
    {   url : './audio/Ghostrifter Official - Hot Coffee.mp3' , 
        cover : './cover/cover-1.jpg', artist : 'Ghostrifter' , title : 'Hot Coffee'},
    {   url : './audio/Ghostrifter Official - Merry Bay.mp3' , 
        cover : './cover/cover-2.jpg', artist : 'Ghostrifter' , title : 'Merry Bay'},
    {   url : './audio/Ghostrifter Official - Morning Routine.mp3' , 
        cover : './cover/cover-4.jpg', artist : 'Ghostrifter' , title : 'Morning Routine'},
    {   url : './audio/Ghostrifter Official - Afternoon Nap.mp3' , 
        cover : './cover/cover-6.jpg', artist : 'Ghostrifter' , title : 'Afternoon Nap'},
    {   url : './audio/Ghostrifter Official - Demised To Shield.mp3' , 
        cover : './cover/cover-7.jpg', artist : 'Ghostrifter' , title : 'Demised To Shield'},
    {   url : './audio/Ghostrifter Official - Departure.mp3' , 
        cover : './cover/cover-8.jpg', artist : 'Ghostrifter' , title : 'Departure'},
    {   url : './audio/Ghostrifter Official - Mellow Out.mp3' , 
        cover : './cover/cover-9.jpg', artist : 'Ghostrifter' , title : 'Mellow Out'},
    {   url : './audio/Ghostrifter Official - On My Way.mp3' , 
        cover : './cover/cover-10.jpg', artist : 'Ghostrifter' , title : 'On My Way'},
    {   url : './audio/Ghostrifter Official - Simplicit Nights.wav' , 
        cover : './cover/cover-1.jpg', artist : 'Ghostrifter' , title : 'Simplicit Nights'},
    {   url : './audio/Ghostrifter Official - Soaring.mp3' , 
        cover : './cover/cover-2.jpg', artist : 'Ghostrifter' , title : 'Soaring'},
    {   url : './audio/Ghostrifter Official - Subtle Break.mp3' , 
        cover : './cover/cover-3.jpg', artist : 'Ghostrifter' , title : 'Subtle Break'},
    {   url : './audio/Ghostrifter Official - Transient.mp3' , 
        cover : './cover/cover-4.jpg', artist : 'Ghostrifter' , title : 'Transient'},
]

let track = document.querySelector('#track')

let playBtn = document.querySelector('#play-btn')
let pauseBtn = document.querySelector('#pause-btn')

let playBtn2 = document.querySelector('#play-btn2')
let pauseBtn2 = document.querySelector('#pause-btn2')

let prevBtn = document.querySelector('#prev-btn')
let nextBtn = document.querySelector('#next-btn')

let sidebarToggle = document.querySelector('#sidebar-toggle')
let likeBtn = document.querySelector('#like-btn')
let playerToggle = document.querySelector('#player-toggle')

let trackArtist = document.querySelector('#track-artist')
let trackTitle = document.querySelector('#track-title')
let trackCover = document.querySelector('#track-cover')

let slider = document.querySelector('#duration_slider');

let recent_volume= document.querySelector('#volume');
let volume_show = document.querySelector('#volume_show');
let volume_icon = document.querySelector('#volume_icon');
let mute_icon = document.querySelector('#mute_icon');

let currentTime = document.querySelector('#current-time')
let totalTime = document.querySelector('#total-time')

let sideCurrentTime = document.querySelector('#side-current-time')
let sideTotalTime = document.querySelector('#side-total-time')

let playing = false
let currentTrack = 0
let volume = track.volume

function play() {

    if(!playing){
        playBtn.classList.add('d-none','pulsate-violet')
        pauseBtn.classList.remove('d-none')
        pauseBtn.classList.add('pulsate-violet')
        playBtn2.classList.add('d-none','pulsate-violet')
        pauseBtn2.classList.remove('d-none')
        pauseBtn2.classList.add('pulsate-violet')
        changePlaylistActive()
        track.play()
        playing = true
    } else {
        playBtn.classList.remove('d-none')
        pauseBtn.classList.add('d-none')
        playBtn2.classList.remove('d-none')
        pauseBtn2.classList.add('d-none')
        track.pause()
        playing = false
    }
}

function next(){
    nextBtn.classList.toggle('pulsate-violet')
    currentTrack++

    if(currentTrack > tracks.length - 1) {
        currentTrack = 0
    }
    console.log(currentTrack)

    changeTrackDetails()
    changePlaylistActive()

    if(playing) {
        playing = false
        play()
    }
}

function prev(){
    prevBtn.classList.toggle('pulsate-violet')
    currentTrack--

    if(currentTrack < 0) {
        currentTrack = tracks.length - 1
    }
    console.log(currentTrack)

    changeTrackDetails()
    changePlaylistActive()

    if(playing) {
        playing = false
        play()
    }
}

function changeTrackDetails() {
    track.src = tracks[currentTrack].url
    // trackArtist.innerHTML = tracks[currentTrack].artist
    trackTitle.innerHTML = tracks[currentTrack].title
    trackCover.src = tracks[currentTrack].cover
}

function changePlaylistActive() {
    let trackListCards = document.querySelectorAll('.border-main')
    trackListCards.forEach((card, index) => {
        if(index == currentTrack) {
            card.classList.add('active')
        } else {
            card.classList.remove('active')
        }
    })
}

function closeSidebar() {
    let siderbar = document.querySelector('#sidebar')
    sidebarToggle.classList.toggle('rotate')

    siderbar.classList.toggle('close')
    sidebarToggle.classList.toggle('pulsate-green')
    let wrapper = document.querySelector('#tracklist-wrapper')
    wrapper.scrollIntoView({block: "end", inline: "nearest"});
}

function populateTracklist() {
    let wrapper = document.querySelector('#tracklist-wrapper')

    tracks.forEach((track, index) => {

        let card = document.createElement('div')

        card.classList.add('row', 'text-center', 'text-white', 'd-flex', 'align-items-center', 'my-2', 'selected', 'border-main')

        card.innerHTML = 
        `
        <audio class="tempi" src="${track.url}"></audio>
        <div class="col-3 d-flex justify-content-center align-items-center">
        <p class="text-white me-3">${index +1}</p>
        <img data-track="${index}" class="img-fluid thumbnail pointer playlist-play" src="${track.cover}">
        </div>
        <div class="col-3">${track.artist}</div>
        <div class="col-3">${track.title}</div>
        <div id="tempo${index}" class="col-3">3.30</div>
        `

        wrapper.appendChild(card)
    })

    let selectedTrack = document.querySelectorAll('.playlist-play')

    selectedTrack.forEach(selected => {
    selected.addEventListener('click', function(){
        let trackPlaying = selected.getAttribute('data-track')

        currentTrack = trackPlaying

        changeTrackDetails()
        changePlaylistActive()
    
        if(playing) {
            playing = false
            play()
        }
    })
})

}

populateTracklist()

function tempistiche (){
    let tempi = document.querySelectorAll('.tempi')    
let audio = ""
   tempi.forEach((time, i) => {
 
    audio = formatTime(time.duration)

   document.getElementById("tempo" + i).innerHTML=audio;
    })   
}

setInterval (function(){
    currentTime.innerHTML = formatTime(track.currentTime)
    totalTime.innerHTML = formatTime(track.duration)
}, 900)

function formatTime (sec) {
    let minutes = Math.floor(sec / 60);
    let seconds = Math.floor(sec - minutes * 60);
    if (seconds < 10) {
        seconds = `0.${seconds}`;
    } return `${minutes}.${seconds}`
}

let timer;

function load_track(){
	clearInterval(timer);
	reset_slider();

    track.load();

	timer = setInterval(range_slider ,1000);
}

load_track();


// reset song slider
 function reset_slider(){
 	slider.value = 0;
 }


// change slider position 
function change_duration(){
	slider_position = track.duration * (slider.value / 100);
	track.currentTime = slider_position;
}

function range_slider(){
	let position = 0;
        
        // update slider position
		if(!isNaN(track.duration)){
		   position = track.currentTime * (100 / track.duration);
		   slider.value =  position;
	      }
}

function change_volume() {
    volume_show.innerHTML = recent_volume.value;
    track.volume = recent_volume.value / 100;
}

function mute_sound(){
    track.volume = 0;
    volume.value = 0;
    recent_volume.value = 0;
    volume_show.innerHTML = 0;
    volume_icon.classList.add('d-none');
    mute_icon.classList.remove('d-none');
}

function unmute_sound() {
    track.volume = 1;
    volume.value = 50;
    recent_volume.value = 50;
    volume_show.innerHTML = 50;
    volume_icon.classList.remove('d-none');
    mute_icon.classList.add('d-none');
}

function animatedLikeBtn() {
    likeBtn.classList.toggle('pulsate-lightblue')
}

playBtn.addEventListener('click', play)
pauseBtn.addEventListener('click', play)
playBtn2.addEventListener('click', play)
pauseBtn2.addEventListener('click', play)
nextBtn.addEventListener('click', next)
prevBtn.addEventListener('click', prev)
track.addEventListener('ended', next)
likeBtn.addEventListener('click', animatedLikeBtn)
sidebarToggle.addEventListener('click', closeSidebar)

window.addEventListener('load', () => {
    tempistiche()
  });

  changeTrackDetails()
  changePlaylistActive()
